﻿init -999 python:
    unl_default_path = 'mods/scenario_unly/'
    def get_unl_sprites(file):
        return unl_default_path + 'images/sprites/%s' % (file)

    def get_un_sprites(file):
        return 'images/sprites/normal/un/%s' % (file)

    def get_unl_cg(file):
        return unl_default_path+"images/cg/%s" % (file)

    def get_unl_bg(file):
        return unl_default_path+"images/bg/%s" % (file)

    def get_unl_gui(file):
        return unl_default_path+"images/gui/%s" % (file)

    def get_unl_sound(file):
        return unl_default_path+"sound/%s" % (file)
    
    def unl_meet(who, name):
        unl_set_name(who,name)

    def unl_set_name(who,name):
        gl = globals()
        global store
        store.names[who] = name
        gl[who+"_name"] = store.names[who]

    def make_names_unknown_unl():
        global store
        unl_meet('unl_unl',u"Странная девочка")
        unl_meet('unl_me',u"Я")
        unl_meet('unl_el',u"Кудрявый")
        unl_meet('unl_un',u"Грустяша")
        unl_meet('unl_dv',u"Рыжая")
        unl_meet('unl_sl',u"Блондинка")
        unl_meet('unl_us',u"Мелкая")
        unl_meet('unl_mt',u"Вожатая")
        unl_meet('unl_mz',u"Очкарик")
        unl_meet('unl_mi',u"Японка")
        unl_meet('unl_sh',u"Шурик")
        unl_meet('mt_voice',u"Голос")
        unl_meet('unl_message',u"Сообщение")
        unl_meet('unl_dy',u"Динамики")
        unl_meet('unl_all',u"Пионеры")
        unl_meet('unl_kids',u"Малышня")
        unl_meet('unl_voices',u"Голоса")
        unl_meet('unl_cs',u"Медсестра")

    def make_names_known_unl():
        global store
        unl_meet('unl_unl',u"Алёна")
        unl_meet('unl_me',u"Семён")
        unl_meet('unl_el',u"Сыроега")
        unl_meet('unl_un',u"Лена")
        unl_meet('unl_dv',u"Алиска")
        unl_meet('unl_sl',u"Славя")
        unl_meet('unl_us',u"Ульянка")
        unl_meet('unl_mt',u"Тётя Оля")
        unl_meet('unl_mz',u"Женя")
        unl_meet('unl_mi',u"Мику-тян")
        unl_meet('unl_sh',u"ШурикЪ")
        unl_meet('mt_voice',u"Голос")
        unl_meet('unl_message',u"Сообщение")
        unl_meet('unl_dy',u"Динамики")
        unl_meet('unl_all',u"Пионеры")
        unl_meet('unl_kids',u"Малышня")
        unl_meet('unl_voices',u"Голоса")
        unl_meet('unl_cs',u"Медсестра")

    def nighted(id):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(-0.2) * im.matrix.saturation(-0.4))
    def nighted2(id):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(-0.4) * im.matrix.saturation(-0.2))
    def sunseted(id):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(-0.1) * im.matrix.tint(0.94, 0.82, 1.0))
    def dayed(id):
        return im.MatrixColor(ImageReference(id), im.matrix.brightness(0.2) * im.matrix.tint(1.0, 0.94, 0.82))

init:
    $ mods["Alena__unly_chan"]=u"История Алёны"
    
    $ mod_tags["Alena__unly_chan"] = ["length:days","gameplay:vn","protagonist:male","character:Виола","character:Алиса","character:Электроник","character:Семён","character:Мику","character:Ольга Дмитриевна","character:Алёна","character:Шурик","character:Славя","character:Лена","character:Ульяна"]
label unl_init_names:
    python:
        for char in ("me el un dv sl us mt mz mi sh cs message mt_voice".split(" ")):
            names["unl_"+char] = names[char]
            colors["unl_"+char] = colors[char]
            store.names_list.append("unl_"+char)
       
        names['unl_unl'] = u'Алёна'
        store.names_list.append('unl_unl')
        colors['unl_unl'] = {'night': (54, 99, 143, 255), 'sunset': (100, 127, 168, 255), 'day': (108, 154, 200, 255), 'prolog': (108, 154, 200, 255)}
#        names['unl_dy'] = u"Динамики"
#        store.names_list.append('unl_dy')
#        names['unl_kids'] = u"Малышня"
#        store.names_list.append('unl_kids')
#        names['unl_voices'] = u"Голоса"
#        store.names_list.append('unl_voices')
        unl_dy = Character(u'Динамики', color="#c0c0ff", what_color="E2C778", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000")
        unl_voices = Character(u'Голоса', color="#c0c0c0", what_color="C0C0C0", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000")
        unl_kids = Character(u'Малышня', color="#eb7883", what_color="E2C778", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000")
        names['unl_all'] = u"Пионеры"
        store.names_list.append('unl_all')
        colors['unl_all'] = colors['all']

        unl_atg = Character(u'Лена|Алиса"', color="#ffaa00", what_color="E2F778", drop_shadow = [ (-1, -1), (1, -1), (-1, 1), (1, 1) ], drop_shadow_color = "#000")
        reload_names()
    return

init:
    $ unl_ata = "В атаку!"
    $ style.unl_choose = Style(style.default)
    $ style.unl_choose.color = "#ad5"
    $ style.unl_choose.drop_shadow = [ (1, 1), (1, 1), (1, 1), (1, 1) ]
    $ style.unl_choose.drop_shadow_color = "#000"
    $ style.unl_choose.italic = False
    $ style.unl_choose.bold = True

    image unl_choose = ParameterizedText(style = "unl_choose", size = 40)

    $ style.unl_choose1 = Style(style.default)
    $ style.unl_choose1.color = "#f33"
    $ style.unl_choose1.drop_shadow = [ (1, 1), (1, 1), (1, 1), (1, 1) ]
    $ style.unl_choose1.drop_shadow_color = "#000"
    $ style.unl_choose1.italic = False
    $ style.unl_choose1.bold = True
    $ style.unl_choose1.text_align = 0.5

    image unl_choose1 = ParameterizedText(style = "unl_choose1", size = 50)

    $ unl_ata2 = "9000+"
    $ unl_scream = "СМЕРТЕЛЬНЫЙ КРИК!"
    $ unl_scream10 = "БРАМ!"
    $ unl_scream11 = "БАМ!"
    $ tobecon = "To be continued…"
#Transition
    $ gspr = ImageDissolve(im.Tile(get_unl_gui("grain1.jpg")), 1.2, 1) #Грязевой телепорт
    $ vspr = ImageDissolve(im.Tile(get_unl_gui("grain11.jpg")), 1.2, 1) #Вертикальный телепорт (идеален для использования со звуком sfx_underwater_dive)
    $ cwspr = ImageDissolve(im.Tile(get_unl_gui("grain13.jpg")), 1.0, 1) #Телепорт диагонально слева снизу - вправо вверх
    $ ccwspr = ImageDissolve(im.Tile(get_unl_gui("grain14.jpg")), 1.0, 1) #Зеркальная версия телепорта выше
    $ hspr = ImageDissolve(im.Tile(get_unl_gui("grain12.jpg")), 2.1, 1) #Горизонтальный, медленный телепорт
    $ flash_light = Fade(0.015, 0.015, 0.27, color="#fff")


    $ th_prefix = "«"
    $ th_suffix = "»"
#CG
    #image cg = get_unl_cg("cg.png")
    image cg d1_unl_owl = get_unl_cg("d1_unl_owl.jpg")
    image cg d1_unl_knight = get_unl_cg("d1_unl_knight.jpg")
    image cg d2_unl_beach = get_unl_cg("d2_unl_beach.jpg")
    image cg d2_unl_chair = get_unl_cg("d2_unl_chair.jpg")
    image cg d2_unl_moon = get_unl_cg("d2_unl_moon.jpg")
    image cg d3_unl_dance = get_unl_cg("d3_unl_dance.jpg")
    image cg d4_unl_hug = get_unl_cg("d4_unl_hug.jpg")
    image cg d5_unl_kissing = get_unl_cg("d5_unl_kissing.jpg")
    
    image cg d8_unl_shower = get_unl_cg("d8_unl_shower.jpg")
    
    image cg d10_unl_grown = get_unl_cg("d10_unl_grown.jpg")
    image cg d12_unl_unl_epilogue = get_unl_cg("d12_unl_unl_epilogue.jpg")
    
    image cg d12_unl_dv_epilogue = get_unl_cg("d12_unl_dv_epilogue.jpg")
    image cg d12_unl_un_epilogue = get_unl_cg("d12_unl_un_epilogue.jpg")
    
    image cg d12_unl_bad = get_unl_cg("d12_unl_bad.jpg")
    image cg d12_unl_gameover = get_unl_cg("d12_unl_gameover.jpg")
    
#BG   
    image bg ext_moon_forest = get_unl_bg("ext_moon_forest.jpg")
    image bg ext_showers_day = get_unl_bg("ext_showers_day.png")
    image bg ext_tennis_court_day = get_unl_bg("ext_tennis_court_day.png")
    image bg ext_aidpost_sunset = get_unl_bg("ext_aidpost_sunset.jpg")
    image bg ext_boathouse_sunset = get_unl_bg("ext_boathouse_sunset.jpg")
    image bg ext_dining_hall_people_sunset = get_unl_bg("ext_dininghall_people_sunset.jpg")
#Misc
    image lib_table = get_unl_sprites('lib_table.png')
#GUI
    image unl_logo = get_unl_gui("unl_logo.png")
    image unl_anim_grain:
        get_unl_gui("grain1.png")
        0.1 #Задержка
        get_unl_gui("grain2.png")
        0.1
        get_unl_gui("grain3.png")
        0.1
        get_unl_gui("grain2.png")
        0.1
        repeat


#BGM
    $ strange_girl = get_unl_sound("strange_girl.ogg")
    $ thelastdragon = get_unl_sound("thelastdragon.ogg")

#Ambience:
    $ ambience_night_party = get_unl_sound("ambience_night_party.ogg")
#Sprites

#Спрайтов список.
#Абсолютно все эмоции Унылки с купальником swim2 (cry, cry_smile, guilty, shocked, surprise, scared без бюстальтера).
# Эмоции с суффиксом 1 на тело 1 (включает dress): dontlike, grin(n/f/c), normal(n/f/c), guilty(n/f/c), serious(n/f/c), smile(n/f/c)
# Эмоции с суффиксом 2 на тело 2 (только pioneer): grin2 (n/f/c), sorrow, dull(n/f/c)
# Эмоции с суффкисом 3 на тело 3 (включает dress и swim): angry, sad, wonder, upset

#Composite:
#UN swim2:
    #normal/un_swim2_1.png
    #normal/un_swim2_2_no_bra.png
    #normal/un_swim2_3.png

    image un angry swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_angry.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_angry.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_angry.png')))

    image un evil_smile swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_evil_smile.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_evil_smile.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_evil_smile.png')))

    image un normal swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_normal.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_normal.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_normal.png')))

    image un shy swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_shy.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_shy.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_shy.png')))

    image un smile swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_smile.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_smile.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_smile.png')))

    image un smile2 swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_smile2.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_smile2.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_1_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_1.png'), (0, 0), get_un_sprites('un_1_smile2.png')))

    image un cry swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_cry.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_cry.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_cry.png')))

    image un cry_smile swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_cry_smile.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_cry_smile.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_cry_smile.png')))

    image un sad swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_sad.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_sad.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_sad.png')))

    image un scared swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_scared.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_scared.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_scared.png')))

    image un shocked swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_shocked.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_shocked.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_shocked.png')))

    image un surprise swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_surprise.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_surprise.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_2_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_2_no_bra.png'), (0, 0), get_un_sprites('un_2_surprise.png')))

    image un angry2 swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_angry2.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_angry2.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_angry2.png')))

    image un grin swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_grin.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_grin.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_grin.png')))

    image un laugh swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_laugh.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_laugh.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_laugh.png')))

    image un rage swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_rage.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_rage.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_rage.png')))

    image un serious swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_serious.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_serious.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_serious.png')))

    image un smile3 swim2 = ConditionSwitch("persistent.sprite_time=='sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_smile3.png')), im.matrix.tint(0.94, 0.82, 1.0)), "persistent.sprite_time=='night'", im.MatrixColor(im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_smile3.png')), im.matrix.tint(0.63, 0.78, 0.82)), True, im.Composite((1050, 1080),  (0, 0), get_un_sprites('un_3_body.png'), (0, 0), get_unl_sprites('normal/un_swim2_3.png'), (0, 0), get_un_sprites('un_3_smile3.png')))

#UNL
    image unl normal pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/normal.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/normal.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/normal.png')) )

    image unl serious pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/serious.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/serious.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/serious.png')) )

    image unl wonder pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/wonder.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/wonder.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/wonder.png')) )

    image unl wonder dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/wonder.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/wonder.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/wonder.png')) )

    image unl wonder dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/wonder.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/wonder.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/wonder.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')) )

    image unl wonder swim = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/wonder.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/wonder.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/wonder.png')) )

    image unl grin pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/grin.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/grin.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/grin.png')) )

    image unl guilty pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/guilty.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/guilty.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/guilty.png')) )

    image unl smile pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/smile.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/smile.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/smile.png')) )

    image unl normal pioneer far = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/normal.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/normal.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/normal.png')) )

    image unl serious pioneer far = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/serious.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/serious.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/serious.png')) )

    image unl grin pioneer far = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/grin.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/grin.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/grin.png')) )

    image unl guilty pioneer far = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/guilty.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/guilty.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/guilty.png')) )

    image unl smile pioneer far = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/smile.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/smile.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('far/body.png'), (0,0), get_unl_sprites('far/smile.png')) )

    image unl normal pioneer close = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/normal.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/normal.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/normal.png')) )

    image unl serious pioneer close = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/serious.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/serious.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/serious.png')) )

    image unl grin pioneer close = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/grin.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/grin.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/grin.png')) )

    image unl guilty pioneer close = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/guilty.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/guilty.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/guilty.png')) )

    image unl smile pioneer close = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/smile.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/smile.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('close/body.png'), (0,0), get_unl_sprites('close/smile.png')) )

    image unl grin2 pioneer close = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body2.png'), (0,0), get_unl_sprites('close/grin2.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body2.png'), (0,0), get_unl_sprites('close/grin2.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('close/body2.png'), (0,0), get_unl_sprites('close/grin2.png')) )

    image unl dull pioneer close = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body2.png'), (0,0), get_unl_sprites('close/dull.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('close/body2.png'), (0,0), get_unl_sprites('close/dull.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('close/body2.png'), (0,0), get_unl_sprites('close/dull.png')) )

    image unl grin2 pioneer far = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body2.png'), (0,0), get_unl_sprites('far/grin2.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body2.png'), (0,0), get_unl_sprites('far/grin2.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('far/body2.png'), (0,0), get_unl_sprites('far/grin2.png')) )

    image unl dull pioneer far = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body2.png'), (0,0), get_unl_sprites('far/dull.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('far/body2.png'), (0,0), get_unl_sprites('far/dull.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('far/body2.png'), (0,0), get_unl_sprites('far/dull.png')) )

    image unl grin2 pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body2.png'), (0,0), get_unl_sprites('normal/grin2.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body2.png'), (0,0), get_unl_sprites('normal/grin2.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body2.png'), (0,0), get_unl_sprites('normal/grin2.png')) )

    image unl dull pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body2.png'), (0,0), get_unl_sprites('normal/dull.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body2.png'), (0,0), get_unl_sprites('normal/dull.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body2.png'), (0,0), get_unl_sprites('normal/dull.png')) )

    image unl dontlike pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/dontlike.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/dontlike.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/dontlike.png')) )

    image unl angry pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/angry.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/angry.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/angry.png')) )

    image unl angry dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/angry.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/angry.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/angry.png')) )

    image unl angry dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/angry.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/angry.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/angry.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')) )

    image unl angry swim = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/angry.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/angry.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/angry.png')) )

    image unl sad pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/sad.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/sad.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/sad.png')) )

    image unl sad dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/sad.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/sad.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/sad.png')) )

    image unl sad dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/sad.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/sad.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/sad.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')) )

    image unl sad swim = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/sad.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/sad.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/sad.png')) )

    image unl sorrow pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/sorrow.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/sorrow.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body.png'), (0,0), get_unl_sprites('normal/sorrow.png')) )

    image unl sorrow pioneer flipped = Transform("unl sorrow pioneer", xzoom=-1.0)

    image unl sorrow dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/sorrow.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/sorrow.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/sorrow.png')) )

    image unl sorrow dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/sorrow.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/sorrow.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/sorrow.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')) )

    image unl upset pioneer = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/upset.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/upset.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/body3.png'), (0,0), get_unl_sprites('normal/upset.png')) )

    image unl upset swim = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/upset.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/upset.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/bikini.png'), (0,0), get_unl_sprites('normal/upset.png')) )

    image unl upset dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/upset.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/upset.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/upset.png')) )

    image unl upset dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/upset.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/upset.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress.png'), (0,0), get_unl_sprites('normal/upset.png'), (0,0), get_unl_sprites('normal/bunny_ears.png')) )

    image unl normal dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/normal.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/normal.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/normal.png')) )

    image unl normal dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/normal.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/normal.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/normal.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')) )

    image unl serious dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/serious.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/serious.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/serious.png')) )

    image unl serious dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/serious.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/serious.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/serious.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')) )

    image unl grin dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/grin.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/grin.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/grin.png')) )

    image unl grin dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/grin.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/grin.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/grin.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')) )

    image unl guilty dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/guilty.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/guilty.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/guilty.png')) )

    image unl guilty dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/guilty.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/guilty.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/guilty.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')) )

    image unl smile dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/smile.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/smile.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/smile.png')) )

    image unl smile dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/smile.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/smile.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/smile.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')) )

    image unl dontlike dress = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/dontlike.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/dontlike.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/dontlike.png')) )

    image unl dontlike dress bunny_ears = ConditionSwitch("persistent.sprite_time=='sunset'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/dontlike.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.94, 0.82, 1.0)),
    "persistent.sprite_time=='night'",im.MatrixColor(im.Composite((900, 1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/dontlike.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')), im.matrix.tint(0.63, 0.78, 0.82)),
    True,im.Composite((900,1080), (0,0), get_unl_sprites('normal/dress1.png'), (0,0), get_unl_sprites('normal/dontlike.png'), (0,0), get_unl_sprites('normal/bunny_ears1.png')) )

#Shades

    image unl shade = im.MatrixColor(im.Composite((900, 1080), (0, 0), get_unl_sprites('normal/body.png'), (0, 0), get_unl_sprites('normal/normal.png')), im.matrix.tint(0.01, 0.01, 0.01) )
    
    transform zenright: #зум-энтер вправо
        xalign 0.5 yalign 0.5 zoom 1.0
        linear 0.4 zoom 1.1 xalign 0.7 yalign 0.5
        
    transform enright: #пан вправо
        xalign 0.5 yalign 0.5 zoom 1.1
        linear 0.5 zoom 1.1 xalign 0.9 yalign 0.5
        
    transform zenleft: #зум-энтер влевоа
        xalign 0.5 yalign 0.5 zoom 1.0
        linear 0.4 zoom 1.1 xalign 0.3 yalign 0.5
        
    transform enleft: #пан влево
        xalign 0.5 yalign 0.5 zoom 1.1
        linear 0.5 zoom 1.1 xalign 0.1 yalign 0.5
        
    transform zencenter: #зум-энтер центровой
        xalign 0.5 yalign 0.5 zoom 1.0
        linear 0.5 zoom 1.15 xalign 0.5 yalign 0.5
        
    transform zexcenter: #зум-эксит центровой
        xalign 0.5 yalign 0.5 zoom 1.2
        linear 0.5 zoom 1.0 xalign 0.5 yalign 0.5
        
    transform zexright: #отдалаюящий эффект от левого края к центру (полезен только в переходе от near столовой к away)
        xalign 0.7 yalign 0.5 zoom 1.2
        linear 0.4 zoom 1.0 xalign 0.5 yalign 0.5
        
    transform zexleft: #отдаляющий эффект от правого края к центру 
        xalign 0.3 yalign 0.5 zoom 1.2
        linear 0.4 zoom 1.0 xalign 0.5 yalign 0.5

